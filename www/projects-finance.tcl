# /packages/intranet-reporting/projects-finance
#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# ---------------------------------------------------------------
# Page Contract
# ---------------------------------------------------------------

ad_page_contract {
   Show a table with
   
   Projektnummer
      Projekttyp
      Projektleiter
      Rechnungssumme aus 2016. Hier werden alle KundenRechnungen gezählt welche ein Rechnungsdatum im Jahr 2016 haben. Falls Ihr Rechnungen ausserhalb ]project-open[ erstellt kann ich diese nicht finden.
      Lieferantensumme aus 2016. Hier werden alle Provider Bills gezählt welche mit dem Projekt verknüpft sind. Wenn Ihr sonstige Rechnungen bekommt und diese nicht mit dem Projekt verknüpft sind kann ich die auch nicht finden.
      Geloggte Stunden   

    @author Malte Sussdorff (malte.sussdorff@cognovis.de)
} {
    { end_date "" }
    { start_date "" }
}


set current_user_id [auth::require_login]
set admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]
set subsite_id [ad_conn subsite_id]
set site_url "/intranet-timesheet2"
set return_url "timesheet-projects"
set date_format "YYYY-MM-DD"
set name_order [parameter::get -package_id [apm_package_id_from_key intranet-core] -parameter "NameOrder" -default 1]

if {"" == $start_date} { 
    set start_date [db_string get_today "select to_char(sysdate - interval '10 weeks',:date_format) from dual"]
}

if {"" == $end_date} { 
    # if no end_date is given, set it to six months in the future
    set end_date [db_string current_month "select to_char(sysdate + interval '1 week',:date_format) from dual"]
}

# Get the first and last month
set start_month [db_string start_month "select to_char(to_date(:start_date,'YYYY-MM-DD'),'YYMM') from dual"]
set end_month [db_string end_month "select to_char(to_date(:end_date,'YYYY-MM-DD'),'YYMM') from dual"]

# ---------------------------------------------------------------
# 3. Defined Table Fields
# ---------------------------------------------------------------

# Define the column headers and column contents that 
# we want to show:
#

im_view_set_def_vars -view_name $view_name -array_name "view_arr" -order_by $order_by -url "[export_vars -base "hours_report" -url {owner_id project_id cost_center_id end_date start_date approved_only_p workflow_key view_name dimension display_type}]"

set __column_defs ""
set __header_defs ""
foreach column_header $view_arr(column_headers) {
    append __column_defs "<table:table-column table:style-name=\"co1\" table:default-cell-style-name=\"ce3\"/>\n"
    append __header_defs " <table:table-cell office:value-type=\"string\"><text:p>$column_header</text:p></table:table-cell>\n"
}


# ---------------------------------------------------------------
# Get the Data and fill it up into lists
# ---------------------------------------------------------------

# Default to all active employees
set user_list [db_list employees "select employee_id from im_employees where employee_status_id = [im_employee_status_active] order by im_name_from_user_id(employee_id,3)"]

# Filter by owner_id
if {$owner_id != ""} {
    lappend view_arr(extra_wheres) "h.user_id = :owner_id"
    set user_list $owner_id
}    

# Filter for projects
if {$filter_project_id != ""} {
    
    if {$pm_manager_p || [im_permission $current_user_id "view_hours_all"]} {
    
        # Get all hours for this project, including hours logged on
        # tasks (100) or tickets (101)
        lappend view_arr(extra_wheres) "(h.project_id in (	
            select p.project_id
    		from   im_projects p, im_projects parent_p
            where  parent_p.project_id = :filter_project_id
            and    p.tree_sortkey between parent_p.tree_sortkey and tree_right(parent_p.tree_sortkey)
            and    p.project_status_id not in (82)
    		))"
	
    	# We need to look at all current members of this project
        set user_list [im_biz_object_member_ids $filter_project_id]
        foreach subproject_id [im_project_subproject_ids -project_id $filter_project_id] {
            foreach employee_id [im_biz_object_member_ids $subproject_id] {
                if {[lsearch $user_list $employee_id] < 0} {
                    lappend user_list $employee_id
                }
            }
        }
    } else {
        # Trying to access a project even though we are not a pm
        set user_list [list]
        lappend view_arr(extra_wheres) "1=0"
    }
}

# ---------------------------------------------------------------
# Prepare the headers in HTML & xlsx
# ---------------------------------------------------------------

im_view_process_def_vars -array_name view_arr
set table_header_html $view_arr(table_header_html)


foreach timescale_header $timescale_headers {
    append table_header_html "<td class=rowtitle align='center'>$timescale_header</td>"
    # for xlsx output
    if {"percentage" == $dimension} {
    append __column_defs "<table:table-column table:style-name=\"co2\" table:default-cell-style-name=\"ce6\"/>\n"
    } else {
    append __column_defs "<table:table-column table:style-name=\"co2\" table:default-cell-style-name=\"ce5\"/>\n"
    }
    append __header_defs " <table:table-cell office:value-type=\"string\"><text:p>$timescale_header</text:p></table:table-cell>\n"
}

set table_body_html ""

# For xlsx
set __output $__column_defs
# Set the first row
append __output "<table:table-row table:style-name=\"ro1\">\n$__header_defs</table:table-row>\n"

# ---------------------------------------------------------------
#  Define the calculation for the hours based on detail level 
#  and approval
# ---------------------------------------------------------------

    
set possible_projects_sql " (select distinct user_id,project_id from (select user_id,p.project_id from im_hours h, im_projects p where p.project_id=h.project_id and p.project_type_id not in (100,101) UNION select user_id,parent_id from im_hours h, im_projects p where p.project_id = h.project_id and p.project_type_id in (100,101) ) possi)"

# set user_list [list]
set project_ids [list]
db_foreach projects_info_query "select im_name_from_user_id(employee_id, $name_order) as user_name,project_name,personnel_number,p.project_id,employee_id,h.user_id,project_nr,company_id, project_type_id
$view_arr(extra_selects_sql)
from im_projects p, im_employees e, users u,$possible_projects_sql h
$view_arr(extra_froms_sql)
where u.user_id = h.user_id
and p.project_id = h.project_id
and e.employee_id = h.user_id
$view_arr(extra_wheres_sql)
group by username,project_name,personnel_number,employee_id,h.user_id,p.project_id,project_nr,company_id, project_type_id
$view_arr(extra_group_by_sql)
order by $order_by
" {
    #  If we have a ticket or a task we should not show this as a potential project in the report
    # Therefore we will search for the parent_id which must be a project.
    if {$project_type_id eq 100 || $project_type_id eq 101} {
        # Task or ticket, aggregate
        set project_id [db_string parent "select parent_id from im_projects where project_id = :project_id" -default $project_id]
    }

    if {[lsearch $user_list $employee_id] < 0} {
        lappend user_list $employee_id
        set user_projects($employee_id) [list]
    }
    lappend user_projects($employee_id) $project_id
    lappend project_ids $project_id
}

set project_ids [lsort -unique $project_ids] 

switch $detail_level {
    single {
        if {$filter_project_id ne ""} {
            set project_ids $filter_project_id
            set subproject_sql "and project_id in (	
                                    select p.project_id
                                    from im_projects p, im_projects parent_p
                                    where parent_p.project_id = :project_id
                                    and p.tree_sortkey between parent_p.tree_sortkey and tree_right(parent_p.tree_sortkey)
                                    and p.project_status_id not in (82)
                                )"
        } else {
            set project_ids 1 ; # Special project_id to not display the project information
            set subproject_sql ""
        }
    }
    subprojects {
        set subproject_sql "and project_id in (	
            select p.project_id
            from im_projects p, im_projects parent_p
            where parent_p.project_id = :project_id
            and p.tree_sortkey between parent_p.tree_sortkey and tree_right(parent_p.tree_sortkey)
            and p.project_status_id not in (82)
        )"
        if {$filter_project_id eq ""} {
            set parent_ids [im_parent_projects -project_ids $project_ids]
            if {$parent_ids eq ""} {set parent_ids 1} ; # Special project_id to not display the project information
            set project_ids [db_list project_ids "select project_id from im_projects where parent_id is null and project_type_id not in (100,101) and project_id in ([template::util::tcl_to_sql_list $parent_ids])"]
        } else {
         #   set project_ids [db_list project_ids "select project_id from im_projects where parent_id = :filter_project_id and project_type_id not in (100,101) union select :filter_project_id from dual"]
        }
    }
    detailed {
        # Only show hours logged on the project itself or the timesheet tasks below it
        set subproject_sql "and project_id in (	
            select p.project_id
            from im_projects p
            where parent_id = :project_id
            and p.project_status_id not in (82)
            and p.project_type_id in (100,101)
            UNION select :project_id from dual
        )"
    }
}

# Approved comes from the category type "Intranet Timesheet Conf Status"
if {$approved_only_p && [apm_package_installed_p "intranet-timesheet2-workflow"]} {
    set timescale_value_sql "select sum(hours) as sum_hours,$timescale_sql as timescale_header, user_id
                            from im_hours, im_timesheet_conf_objects tco
                            where tco.conf_id = im_hours.conf_object_id and tco.conf_status_id = 17010
                            $subproject_sql
                            and day >= :start_date
                            and day <= :end_date
                            group by user_id,timescale_header
                            order by user_id,timescale_header
                          "
} else {
    set timescale_value_sql "select sum(hours) as sum_hours,$timescale_sql as timescale_header,user_id
                             from im_hours
                             where day >= :start_date
                            and day <= :end_date
                            $subproject_sql
                            group by user_id,timescale_header
                            order by user_id,timescale_header
                          "
}


# If we want the percentages, we need to 
# Load the total hours a user has logged in case we are looking at the
# actuals or forecast

# Approved comes from the category type "Intranet Timesheet Conf Status"
if {$approved_only_p && [apm_package_installed_p "intranet-timesheet2-workflow"]} {
    set hours_sql "select sum(hours) as total, $timescale_sql as timescale_header, user_id
	from im_hours, im_timesheet_conf_objects tco
        where tco.conf_id = im_hours.conf_object_id and tco.conf_status_id = 17010
        and day >= :start_date
        and day <= :end_date
	group by user_id, timescale_header"
} else {
    set hours_sql "select sum(hours) as total, $timescale_sql as timescale_header, user_id
	from im_hours
	where day >= :start_date
	and day <= :end_date
	group by user_id, timescale_header"
}

if {"percentage" == $dimension} {
    db_foreach logged_hours $hours_sql {
        	if {$user_id != "" && $timescale_header != ""} {
	       set user_hours_${timescale_header}_${user_id} $total
	   }
    }
}

# Run through each combination of user and projec to retrieve the
# values

# Load the user - project - timescale into an array

# ---------------------------------------------------------------
# Overwrite the projects in case we have single or summary view.
# ---------------------------------------------------------------

foreach project_id $project_ids {
    db_foreach timescale_info $timescale_value_sql {
        set project_hours(${user_id}-$project_id) $sum_hours
        set var ${user_id}_${project_id}($timescale_header)
        if {"percentage" == $dimension} {
            if {[info exists user_hours_${timescale_header}_$user_id]} {
                set total [set user_hours_${timescale_header}_$user_id]
            } else {
                set total 0
            }
            if {0 < $total} {
                set $var "[expr round($sum_hours / $total *100)]"
            } else {
                set $var "0"
            }
        } else {
            set $var $sum_hours
        }        
    }    
}


if {"xlsx" == $display_type} {
    # Check if we have the table.ods file in the proper place
    set ods_file "[acs_package_root_dir "intranet-openoffice"]/templates/table.ods"
    if {![file exists $ods_file]} {
        ad_return_error "Missing ODS" "We are missing your ODS file $ods_file . Please make sure it exists"
    }
    set table_name "weekly_hours"
    intranet_oo::parse_content -template_file_path $ods_file -output_filename "weekly_hours.xlsx"
    ad_script_abort

} else {
    set left_navbar_html "
            <div class=\"filter-block\">
                $filter_html
            </div>
    "
}

# ---------------------------------------------------------------
# Display the name of the users which are not included in the list
# ---------------------------------------------------------------

set hidden_users_html "<h2>[_ intranet-reporting.users_without_logged_hours]</h2><ul>"
set hidden_user_ids [list]

foreach user_id $user_list {
    if {[lsearch $displayed_user_ids $user_id]<0} {
        lappend hidden_user_ids $user_id
    }
}

if {[llength $hidden_user_ids]>0} {
    db_foreach hidden_users "select employee_id,im_name_from_user_id(employee_id, $name_order) as user_name from im_employees where employee_id in ([template::util::tcl_to_sql_list $hidden_user_ids]) and employee_status_id = [im_employee_status_active] order by user_name" {
        set hidden_user_url [export_vars -base "/intranet/users/view" -url {{user_id $employee_id}}]
        append hidden_users_html "<li><a href='$hidden_user_url'>$user_name</li>"
    }
}
